// 1 Create a Single Room

db.users.insertOne(
	{
		name : "Single",
		accomodates : 2,
		price : 1000,
		description : "A single room with all the basic necessities",
		rooms_available : 10,
		isAvailable : false
	}
);

// 2 Insert Multiple Rooms 

db.users.insertMany([
	{
		name : "Double",
		accomodates : 3,
		price : 2000,
		description : "A room fit for a small family going on a vacation",
		rooms_available : 5,
		isAvailable : false
	},
	{
		name : "Queen",
		accomodates : 4,
		price : 4000,
		description : "A room with a queen sized bed perfect for a simple getaway",
		rooms_available : 15,
		isAvailable : false
	}
]);

// 3 Search a room with a name double

db.users.find({name: "Double"});

// 4 Search for queen room and make available rooms to zero using updateOne


db.users.updateOne(
	{
		name : "Queen"
	},
	{
		$set : {
			rooms_available : 0,
				}
	}
);

// 5 delete rooms that have 0 using deletemany method


db.users.deleteMany({rooms_available: 0});